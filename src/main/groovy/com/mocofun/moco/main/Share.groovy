package com.mocofun.moco.main

import com.alibaba.fastjson.JSONObject
import com.funtester.utils.ArgsUtil
import com.mocofun.moco.MocoServer

class Share extends MocoServer {

    static void main(String[] args) {
        def util = new ArgsUtil(args)
        def server = getServerNoLog(util.getIntOrdefault(0, 8080))
        //                def server = getServer(util.getIntOrdefault(0, 12345))
        //                server.get(both(urlStartsWith("/test"),existArgs("code"))).response("get请求")
        //                server.post(both(urlStartsWith("/test"), existForm("fun"))).response("post请求form表单")
        //                server.post(both(urlStartsWith("/test"), existParams("fun"))).response("post请求json表单")
        //        server.get(urlStartsWith("/qps")).response(qps(textRes("恭喜到达QPS!"), 1))
        //                server.response(delay(jsonRes(getJson("Have=Fun ~ Tester ！")), 100))
        //        server.get(urlOnly("/get")).response(jsonRes(getJson("msg=get请求", "code=0")))
        //        server.get(urlOnly("/get1")).response(jsonRes(getJson("msg=get1请求", "code=1")))
        //        server.get(urlOnly("/get2")).response(jsonRes(getJson("msg=get2请求", "code=2")))
        //        server.get(urlOnly("/get3")).response(jsonRes(getJson("msg=get2请求", "code=3")))
        //        server.get(urlOnly("/get4")).response(jsonRes(getJson("msg=get2请求", "code=4")))

        def res = new JSONObject().fluentPut("code", 0).fluentPut("msg", "注册成功").toJSONString()
        server.response(delay(textRes(res), 100))
//        server.response(delay(textRes("FunTester"), 10))
        def run = run(server)
        waitForKey("FunTester")
        run.stop()
    }
}
